var width = window.innerWidth
var height = window.innerHeight

// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

// angle between y-axis and the vector that spans [p1, p2]
let angle = (p1, p2) => Math.atan2(p2[1] - p1[1], p2[0] - p1[0])

// move a point or a triangle by v
let move_thing = (xys, v) => xys.map((e, i) => e + (i%2==0 ? v[0] : v[1]))

// rotate a point or a triangle around point c by theta
function rotate_thing(xys, c, th) {
  let at_origin = move_thing(xys, [-c[0], -c[1]])
  let rotated = at_origin.map((e, i, l) =>
    i%2 == 0
    ? e * cos(th) - l[i+1] * sin(th)  // ys
    : e * cos(th) + l[i-1] * sin(th)  // ys
  )
  return move_thing(rotated, c)
}

// ...I miss python
let range = ((n) => [...Array(n).keys()])

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL)
  colorMode(HSB, 255)
  frameRate(30)
}

function makeBuddies() {
  let mh = height - 400
  let mw = width - 400

  var buddies = []
  for (var y = -mh / 2; y < mh / 2; y += 15) {
    var row = []
    for (var x = -mw / 2; x < mw / 2; x += 10) {
      row.push([x, y])
    }

    buddies.push(row)
  }
  return buddies
}

function drawBuddy(buddy, x) {
  // circle(...buddy, 5)
  let d = (Math.cos(x/ 4) / Math.sin(x / 4)) * 20 
  let c = [fg[0], fg[1], fg[2] + d]
  fill(...c)
  ellipse(...buddy, 7, 3)
}

function updateRow(row, y) {
  return row.map(buddy => updateBuddy(buddy, y))
}

function updateBuddy(buddy, rowno) {
  let x = buddy[0]
  let y = buddy[1]
  let dx = 1/8
  let dy = noise(
    (x / 8) + width,
    rowno / 8
  ) - 0.5;
  var newBuddy = [
    buddy[0] + dx,
    buddy[1] + dy
  ]
  if (newBuddy[0] > width / 2) {
    newBuddy[0] -= width
  }
  if (newBuddy[1] > height / 2) {
    newBuddy[1] -= height
  }
  if (newBuddy[1] < -height / 2) {
    newBuddy[1] += height
  }
  return newBuddy
}

let bg = [109, 87, 100]
let fg = [30, 87, 180]

var buddies = makeBuddies()
function draw() {
  noStroke()
  background(...bg)

  // update
  // buddies = buddies.map(row => row.map(updateBuddy))
  buddies = buddies.map(updateRow)

  // render
  buddies.forEach(row =>
    row.forEach(drawBuddy)
  )
}
